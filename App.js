import React from 'react';
import { StatusBar } from 'react-native';
import { FontReplacers } from 'engine/FontReplacers';
import { NavigationContainer } from '@react-navigation/native';

// import { Provider } from "react-redux";
// import Store from "./src/redux/store/Store";
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from 'reduxs/store/Store';

import InitialNavigator from './src/router/InitialNavigator';

// FontReplacers('NunitoSans');
FontReplacers('Raleway');

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <StatusBar barStyle="light-content" />
          <InitialNavigator />
        </NavigationContainer>
      </PersistGate>
    </Provider>
    // <Provider store={Store}>
    //   <NavigationContainer>
    //     <StatusBar barStyle="light-content" />
    //     <InitialNavigator />
    //   </NavigationContainer>
    // </Provider>
  );
};

export default App;
