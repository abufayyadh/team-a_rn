import axios from 'axios';

const baseUrl = 'http://139.59.124.53:4581';
const apiKey = '94dbd50418671d3c98367ace1e9300e8';
const headers = {
  'Content-Type': 'application/json',
  api_key: `${apiKey}`,
  Authorization: ''
};

export const checkVersion = async (
  data = null,
  header = {
    'Content-Type': 'application/json'
  }
) => {
  let url = `${baseUrl}/api/checkVersion`;
  return await axios.post(url, data, { headers: header }).catch((err) => {
    console.log(err);
  });
};

export const doLogin = async (data = null, header = headers) => {
  let url = `${baseUrl}/api/login`;
  return await axios.post(url, data, { headers: header }).catch((err) => {
    console.log(err);
  });
};

export const doUpdateProfile = async (role, data = null, token = '') => {
  let url = `${baseUrl}/api/${
    role === 'telkomsel' ? 'editprofileram' : 'editprofilepic'
  }`;
  headers.Authorization = token;
  return await axios.post(url, data, { headers: headers });
};

export const getChatList = async (token = '') => {
  let url = `${baseUrl}/api/myCorporate`;
  headers.Authorization = token;
  return await axios.get(url, { headers: headers }).catch((err) => {
    console.log(err);
  });
};

export const getOrderAM = async (token = '') => {
  let url = `${baseUrl}/api/getOrderForAM`;
  headers.Authorization = token;
  return axios.get(url, { headers: headers });
};

export const getOrderItemAM = async (orderId = 0, token = '') => {
  let url = `${baseUrl}/api/getOrderItemsForAM/${orderId}`;
  headers.Authorization = token;
  return axios.request({
    url,
    method: 'get',
    headers: headers,
    params: { order_id: orderId }
  });
};

export const approveRejectOrderAM = async (data = {}, token = '') => {
  let url = `${baseUrl}/api/changeStatusOrder`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const approveRejectOrderItemAM = async (data = [], token = '') => {
  let url = `${baseUrl}/api/changeStatusOrderItem`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const getCorporateAM = async (token = '') => {
  let url = `${baseUrl}/api/myCorporate`;
  headers.Authorization = token;
  return axios.get(url, { headers: headers });
};

export const getBillCorporateAM = async (accountId = 0, token = '') => {
  let url = `${baseUrl}/api/billcorporate?account_id=${accountId}`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getTicketCorporateAM = async (accountId = 0, token = '') => {
  let url = `${baseUrl}/api/ticketbyaccount?account_id=${accountId}`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const addMsisdnAM = async (data = {}, token = '') => {
  let url = `${baseUrl}/api/addMsisdnByAM`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const getProfitHistoryAM = async (token = '') => {
  let url = `${baseUrl}/api/profithistory`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getTicketAM = async (token = '') => {
  let url = `${baseUrl}/api/myTicket`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getOutstandingBillsAM = async (token = '') => {
  let url = `${baseUrl}/api/ramcorpbilling`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getUnpaidBillsAM = async (token = '') => {
  let url = `${baseUrl}/api/ramunpaidcorpbilling`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getTicketDetailAM = async (ticketId = 0, token = '') => {
  let url = `${baseUrl}/api/myticketdetailam?ticket_id=${ticketId}`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const replyTicketAM = async (data = {}, token = '') => {
  let url = `${baseUrl}/api/replyticketam`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const getPackage = async (token = '') => {
  let url = `${baseUrl}/api/listpackage/in_active`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getMsisdnCorporate = async (accountId, token = '') => {
  let url = `${baseUrl}/api/getMsisdn?account_id=${accountId}`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const changePackageCorporate = async (data = [], token = '') => {
  let url = `${baseUrl}/api/changepackagepic`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const addNumberCorporate = async (data = [], token = '') => {
  let url = `${baseUrl}/api/createOrderAddNumbers`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const createTicketCorporate = async (data = [], token = '') => {
  let url = `${baseUrl}/api/createticket`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const getTicketCorporate = async (token = '') => {
  let url = `${baseUrl}/api/ticketuser`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getTicketDetailCorporate = async (ticketId = 0, token = '') => {
  let url = `${baseUrl}/api/myticketdetailpic?ticket_id=${ticketId}`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const replyTicketCorporate = async (data = {}, token = '') => {
  let url = `${baseUrl}/api/replyticketpic`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const closeTicketCorporate = async (data = {}, token = '') => {
  let url = `${baseUrl}/api/closeticket`;
  headers.Authorization = token;
  return axios.request({ url, method: 'put', headers: headers, data });
};

export const getCurrentBillCorporate = async (token = '') => {
  let url = `${baseUrl}/api/piccorpbilling`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getBillCorporate = async (token = '') => {
  let url = `${baseUrl}/api/getBillMonth`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getBillDetailCorporate = async (billId = 0, token = '') => {
  let url = `${baseUrl}/api/getBillDetail?billing_id=${billId}`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const payBillCorporate = async (data = {}, token = '') => {
  let url = `${baseUrl}/api/updatePayment`;
  headers.Authorization = token;
  return axios.request({ url, method: 'put', headers: headers, data });
};

export const getOrderCorporate = async (token = '') => {
  let url = `${baseUrl}/api/getOrderForPIC`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getOrderDetailCorporate = async (orderId = 0, token = '') => {
  let url = `${baseUrl}/api/getOrderItemsForPIC/${orderId}`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const getBillDetailAM = async (billId = 0, token = '') => {
  let url = `${baseUrl}/api/getBillDetail?billing_id=${billId}`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};

export const addNumberAM = async (data = {}, token = '') => {
  let url = `${baseUrl}/api/addMsisdnByAM`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const changePackageAM = async (data = [], token = '') => {
  let url = `${baseUrl}/api/changepackage`;
  headers.Authorization = token;
  return axios.request({ url, method: 'post', headers: headers, data });
};

export const getMsisdnAM = async (accountId = 0, token = '') => {
  let url = `${baseUrl}/api/getMsisdn?account_id=${accountId}`;
  headers.Authorization = token;
  return axios.request({ url, method: 'get', headers: headers });
};
