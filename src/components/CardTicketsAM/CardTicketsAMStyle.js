import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  cardTickets: {
    backgroundColor: Colors.white700,
    borderColor: Colors.gray700,
    borderWidth: 0.5,
    borderRadius: 5,
    marginHorizontal: 10,
    marginTop: 10
  },
  headWrap: {
    flexDirection: 'row',
    padding: 5
  },
  titleHead: {
    marginLeft: 5
  },
  title: {
    color: Colors.red900,
    fontWeight: 'bold'
  },
  subtitle: {
    color: Colors.red700,
    fontWeight: 'bold'
  },
  ticketStatus: {
    flexDirection: 'row'
  },
  textTicket: {
    fontSize: 12,
    marginLeft: 12,
    marginRight: 10,
    width: 200
  },
  company: {
    fontSize: 14,
    color: Colors.red900,
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 5
  },
  statusWrap: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 20
  },
  status: {
    color: Colors.red700
  }
});
