import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { Colors } from '../../utils/ColorReferences';

import { styles } from './CardTicketStyle';

const CardTicket = ({ title, status, content, onPress }) => {
  return (
    <TouchableOpacity
      style={styles.cardTickets}
      onPress={onPress}
    >
      <View style={styles.headWrap}>
        <Icon
          name="ticket-account"
          style={styles.icon}
          size={40}
          color={Colors.red900}
        />
        <View style={styles.titleHead}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subtitle}>Status: {status}</Text>
        </View>
      </View>

      <Text style={styles.textTicket}>{content}</Text>
    </TouchableOpacity>
  );
};

export default CardTicket;
